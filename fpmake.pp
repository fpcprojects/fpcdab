{$mode objfpc}{$H+}
program fpmake;

uses fpmkunit;

var
  P : TPackage;
  T : TTarget;
  D : TDependency;

begin
  with Installer do
    begin
    P:=AddPackage('fpcdab');
    P.Version:='0.5.6-0';

    P.Author:='Joost van der Sluis (CNOC)';
    P.License:='LGPL v2  with static linking exception';
    P.Description:='Free Pascal definitons for Microsoft''s Debug Adapter Protocol.';
    P.HomepageURL := 'https://gitlab.com/fpcprojects/fpcdab';

    D := P.Dependencies.Add('rtl-generics');
    D := P.Dependencies.Add('cerialization');
    T:=P.Targets.AddUnit('dabmessages.pas');
    T:=P.Targets.AddUnit('dabfpdmessages.pas')
    end;
  Installer.Run;
end.
