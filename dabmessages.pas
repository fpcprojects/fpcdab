unit DABMessages;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  csModel,
  csGenericCollectionsDescriber;

type
  TDynIntegerArray = array of Integer;

  { TDABProtocolMessage }

  TDABProtocolMessage = class
  private
    FSeq: Integer;
    FType: string;
  public
    constructor Create; virtual;
    procedure InitSequence;
  published
    property Seq: Integer read FSeq write FSeq;
    property &Type: string read FType write FType;
  end;

  { TDABRequest }

  TDABRequest = class(TDABProtocolMessage)
  private
    FCommand: string;
  protected
    FArguments: TObject;
  public
    constructor create; override;
    destructor Destroy; override;
  published
    property Command: string read FCommand write FCommand;
    property Arguments: TObject read FArguments write FArguments;
  end;

  { TDABResponse }

  TDABResponse = class(TDABProtocolMessage)
  private
    FRequest_seq: Integer;
    FSuccess: Boolean;
    FCommand: string;
    FMessage: string;
    FBody: TObject;
  public
    constructor create; override;
  published
    property Command: string read FCommand write FCommand;
    property Request_seq: Integer read FRequest_seq write FRequest_seq;
    property Success: Boolean read FSuccess write FSuccess;
    property Message: string read FMessage write FMessage;
    property Body: TObject read FBody write FBody;
  end;

  { TDABMessage }

  TDABMessage = class
  private
    FId: integer;
    FFormat: string;
    FSendTelemetry: Boolean;
    FShowUser: Boolean;
    FUrl: string;
    FUrlLabel: string;
  published
    property Id: integer read FId write FId;
    property Format: string read FFormat write FFormat;
    property SendTelemetry: Boolean read FSendTelemetry write FSendTelemetry;
    property ShowUser: Boolean read FShowUser write FShowUser;
    property Url: string read FUrl write FUrl;
    property UrlLabel: string read FUrlLabel write FUrlLabel;
  end;

  { TDABError }

  TDABError = class
  private
    FError: TDABMessage;
    function GetError: TDABMessage;
  public
    destructor Destroy; override;
  published
    property Error: TDABMessage read GetError;
  end;

  { TDABErrorResponse }

  TDABErrorResponse = class(TDABResponse)
  private
    function GetBody: TDABError;
  public
    destructor Destroy; override;
  published
    property Body: TDABError read GetBody;
  end;


  { TDABInitializeRequestArguments }

  TDABInitializeRequestArguments = class
  private
    FAdapterID: string;
  published
    property AdapterID: string read FAdapterID write FAdapterID;
  end;

  { TDABInitializeRequest }

  TDABInitializeRequest = class(TDABRequest)
  private
    function GetArguments: TDABInitializeRequestArguments;
  published
    property Arguments: TDABInitializeRequestArguments read GetArguments;
  end;

  { TDabCapabilities }

  TDABCapabilities = class
  private
    FSupportsConfigurationDoneRequest: boolean;
    FSupportsSetVariable: Boolean;
  published
    property SupportsConfigurationDoneRequest: boolean read FSupportsConfigurationDoneRequest write FSupportsConfigurationDoneRequest;
    property SupportsSetVariable: Boolean read FSupportsSetVariable write FSupportsSetVariable;
  end;

  { TDABLaunchRequestArgumentEnvironmentVariable }

  TDABLaunchRequestArgumentEnvironmentVariable = class
  private
    FName: string;
    FValue: string;
  public
    constructor Create(AName, AValue: string);
  published
    property Name: string read FName write FName;
    property Value: string read FValue write FValue;
  end;
  TDABLaunchRequestArgumentEnvironmentVariableList = specialize TObjectList<TDABLaunchRequestArgumentEnvironmentVariable>;
  TDABLaunchRequestArgumentEnvironmentVariableListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABLaunchRequestArgumentEnvironmentVariable>;

  { TDabLaunchRequestArguments }

  TDABLaunchRequestArguments = class
  private
    FForceNewConsoleWin: Boolean;
    FNoDebug: Boolean;
    FProgram: string;
    FParameters: TStringArray;
    FWorkingDirectory: string;
    FEnvironment: TDABLaunchRequestArgumentEnvironmentVariableList;
    function GetEnvironment: TDABLaunchRequestArgumentEnvironmentVariableList;
  public
    destructor Destroy; override;
  published
    property NoDebug: Boolean read FNoDebug write FNoDebug;
  // FpDebug-specific:
    property &Program: string read FProgram write FProgram;
    property Parameters: TStringArray read FParameters write FParameters;
    property WorkingDirectory: string read FWorkingDirectory write FWorkingDirectory;
    property Environment: TDABLaunchRequestArgumentEnvironmentVariableList read GetEnvironment;
    property ForceNewConsoleWin: Boolean read FForceNewConsoleWin write FForceNewConsoleWin;
  end;

  TDABLaunchRequest = class(TDABRequest)
  private
    function GetArguments: TDABLaunchRequestArguments;
  published
    property Arguments: TDABLaunchRequestArguments read GetArguments;
  end;

  { TDABAttachRequestArguments }

  TDABAttachRequestArguments = class
  private
    FName: string;
  published
    property Name: string read FName write FName;
  end;

  { TDABAttachRequest }

  TDABAttachRequest = class(TDABRequest)
  private
    function GetArguments: TDABAttachRequestArguments;
  published
    property Arguments: TDABAttachRequestArguments read GetArguments;
  end;

  { TDABScopesRequestArguments }

  TDABScopesRequestArguments = class
  private
    FFrameId: Integer;
  published
    property FrameId: Integer read FFrameId write FFrameId;
  end;

  TDABScopesRequest = class(TDABRequest)
  private
    function GetArguments: TDABScopesRequestArguments;
  published
    property Arguments: TDABScopesRequestArguments read GetArguments;
  end;

  TDABScope = class
  private
    FName: string;
    FPresentationHint: string;
    FVariablesReference: Integer;
    FExpensive: Boolean;
  published
    property Name: string read FName write FName;
    property PresentationHint: string read FPresentationHint write FPresentationHint;
    property VariablesReference: Integer read FVariablesReference write FVariablesReference;
    property Expensive: Boolean read FExpensive write FExpensive;
  end;
  TDABScopeList = specialize TObjectList<TDABScope>;
  TDABScopeListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABScope>;

  { TDABScopesResponseBody }

  TDABScopesResponseBody = class
  private
    FScopes: TDABScopeList;
    function GetScopes: TDABScopeList;
  public
    destructor Destroy; override;
  published
    property Scopes: TDABScopeList read GetScopes;
  end;

  TDABScopesResponse = class(TDABResponse)
  private
    function GetBody: TDABScopesResponseBody;
  public
    destructor Destroy; override;
  published
    property Body: TDABScopesResponseBody read GetBody;
  end;

  { TDABSourceBreakpoint }

  TDABSourceBreakpoint = class
  private
    FLine: Integer;
  published
    property Line: Integer read FLine write FLine;
  end;
  TDABSourceBreakpointList = specialize TObjectList<TDABSourceBreakpoint>;
  TDABSourceBreakpointListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABSourceBreakpoint>;

  TDABSource = class
  private
    FName: string;
    FPath: string;
  published
    property Name: string read FName write FName;
    property Path: string read FPath write FPath;
  end;

  { TDABSetBreakpointsArguments }

  TDABSetBreakpointsArguments = class
  private
    FSource: TDabSource;
    FSourceModified: Boolean;
    FBreakpoints: TDABSourceBreakpointList;
    function GetBreakpoints: TDABSourceBreakpointList;
    function GetSource: TDabSource;
  public
    destructor Destroy; override;
  published
    property Source: TDabSource read GetSource;
    property SourceModified: Boolean read FSourceModified write FSourceModified;
    property Breakpoints: TDABSourceBreakpointList read GetBreakpoints;
  end;

  { TDABSetBreakpointsRequest }

  TDABSetBreakpointsRequest =  class(TDABRequest)
  private
    function GetArguments: TDABSetBreakpointsArguments;
  published
    property Arguments: TDABSetBreakpointsArguments read GetArguments;
  end;

  { TDABStackFrame }

  TDABStackFrame = class
  private
    FId: Integer;
    FName: string;
    FSource: TDABSource;
    FLine: Integer;
    FModuleId: string;
    FPresentationHint: string;
    FColumn: Integer;
    procedure SetSource(AValue: TDABSource);
  public
    destructor Destroy; override;
  published
    property Id: Integer read FId write FId;
    property Name: string read FName write FName;
    property Source: TDABSource read FSource write SetSource;
    property Line: Integer read FLine write FLine;
    property ModuleId: string read FModuleId write FModuleId;
    property Column: Integer read FColumn write FColumn;
    property PresentationHint: string read FPresentationHint write FPresentationHint;
  end;
  TDABStackFrameList = specialize TObjectList<TDABStackFrame>;
  TDABStackFrameListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABStackFrame>;

  { TDABStackTraceArguments }

  TDABStackTraceArguments = class
  private
    FThreadId: Integer;
    FStartFrame: Integer;
    FLevels: Integer;
  published
    property ThreadId: Integer read FThreadId write FThreadId;
    property StartFrame: Integer read FStartFrame write FStartFrame;
    property Levels: Integer read FLevels write FLevels;
  end;

  { TDABStackTraceRequest }

  TDABStackTraceRequest =  class(TDABRequest)
  private
    function GetArguments: TDABStackTraceArguments;
  published
    property Arguments: TDABStackTraceArguments read GetArguments;
  end;

  { TDABStackTraceResponseBody }

  TDABStackTraceResponseBody = class
  private
    FTotalFrames: Integer;
    FStackFrames: TDABStackFrameList;
    function GetStackFrames: TDABStackFrameList;
  public
    destructor Destroy; override;
  published
    property TotalFrames: Integer read FTotalFrames write FTotalFrames;
    property StackFrames: TDABStackFrameList read GetStackFrames;
  end;

  TDABStackTraceResponse = class(TDABResponse)
  private
    function GetBody: TDABStackTraceResponseBody;
  public
    destructor Destroy; override;
  published
    property Body: TDABStackTraceResponseBody read GetBody;
  end;

  { TDABEvaluateArguments }

  TDABEvaluateArguments = class
  private
    FExpression: string;
    FFrameId: Integer;
    FContext: string;
  published
    property Expression: string read FExpression write FExpression;
    property FrameId: Integer read FFrameId write FFrameId;
    property Context: string read FContext write FContext;
  end;

  { TDABEvaluateRequest }

  TDABEvaluateRequest = class(TDABRequest)
  private
    function GetArguments: TDABEvaluateArguments;
  published
    property Arguments: TDABEvaluateArguments read GetArguments;
  end;

  { TDABVariablePresentationHint }

  TDABVariablePresentationHint = class
  private
    FKind: string;
    FAttributes: TStringArray;
    FVisibility: string;
  published
    property Kind: string read FKind write FKind;
    property Attributes: TStringArray read FAttributes write FAttributes;
    property Visibility: string read FVisibility write FVisibility;
  end;

  { TDABEvaluateResponseBody }

  TDABEvaluateResponseBody = class
  private
    FResult: string;
    FType: string;
    FVariablesReference: Integer;
    FPresentationHint: TDABVariablePresentationHint;
    function GetPresentationHint: TDABVariablePresentationHint;
  public
    destructor Destroy; override;
  published
    property Result: string read FResult write FResult;
    property &Type: string read FType write FType;
    property VariablesReference: Integer read FVariablesReference write FVariablesReference;
    property PresentationHint: TDABVariablePresentationHint read GetPresentationHint;
  end;

  TDABEvaluateResponse = class(TDABResponse)
  private
    function GetBody: TDABEvaluateResponseBody;
  public
    destructor Destroy; override;
  published
    property Body: TDABEvaluateResponseBody read GetBody;
  end;

  { TDABVariablesArguments }

  TDABVariablesArguments = class
  private
    FVariablesReference: Integer;
    FStart: Integer;
    FCount: Integer;
  published
    property VariablesReference: Integer read FVariablesReference write FVariablesReference;
    property Start: Integer read FStart write FStart;
    property Count: Integer read FCount write FCount;
  end;

  { TDABVariablesRequest }

  TDABVariablesRequest = class(TDABRequest)
  private
    function GetArguments: TDABVariablesArguments;
  published
    property Arguments: TDABVariablesArguments read GetArguments;
  end;

  TDABVariable = class
  private
    FName: string;
    FValue: string;
    FVariablesReference: Integer;
    FType: string;
    FNamedVariables: Integer;
    FIndexedVariables: Integer;
    FPresentationHint: TDABVariablePresentationHint;
    function GetPresentationHint: TDABVariablePresentationHint;
  public
    destructor Destroy; override;
  published
    property Name: string read FName write FName;
    property Value: string read FValue write FValue;
    property &Type: string read FType write FType;
    property VariablesReference: Integer Read FVariablesReference write FVariablesReference;
    property IndexedVariables: Integer read FIndexedVariables write FIndexedVariables;
    property NamedVariables: Integer read FNamedVariables write FNamedVariables;
    property PresentationHint: TDABVariablePresentationHint read GetPresentationHint;
  end;
  TDABVariableList = specialize TObjectList<TDABVariable>;
  TDABVariableListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABVariable>;

  { TDABVariablesResponseBody }

  TDABVariablesResponseBody = class
  private
    FVariables: TDABVariableList;
    function GetVariables: TDABVariableList;

  public
    destructor Destroy; override;
  published
    property Variables: TDABVariableList read GetVariables;
  end;

  TDABVariablesResponse = class(TDABResponse)
  private
    function GetBody: TDABVariablesResponseBody;
  public
    destructor Destroy; override;
  published
    property Body: TDABVariablesResponseBody read GetBody;
  end;

  { TDABSetVariableArguments }

  TDABSetVariableArguments = class
  private
    FVariablesReference: Integer;
    FName: string;
    FValue: string;
  published
    property VariablesReference: Integer read FVariablesReference write FVariablesReference;
    property Name: string read FName write FName;
    property Value: string read FValue write FValue;
  end;

  { TDABSetVariableResponse }

  TDABSetVariableResponse = class
  private
    FValue: string;
    FType: string;
  published
    property Value: string read FValue write FValue;
    property &Type: string read FType write FType;
  end;

  { TDABSetVariableRequest }

  TDABSetVariableRequest = class(TDABRequest)
  private
    function GetArguments: TDABSetVariableArguments;
  published
    property Arguments: TDABSetVariableArguments read GetArguments;
  end;

  { TDABBreakpoint }

  TDABBreakpoint = class
  private
    FLine: Integer;
    FId: Integer;
    FVerified: Boolean;
    FMessage: string;
  published
    property Id: Integer read FId write FId;
    property Verified: Boolean read FVerified write FVerified;
    property Line: Integer read FLine write FLine;
    property Message: string read FMessage write FMessage;
  end;
  TDABBreakpointList = specialize TObjectList<TDABBreakpoint>;
  TDABBreakpointListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABBreakpoint>;

  { TDABBreakpointsResponseBody }

  TDABBreakpointsResponseBody = class
  private
    FBreakpoints: TDABBreakpointList;
    function GetBreakpoints: TDABBreakpointList;
  public
    destructor Destroy; override;
  published
    property Breakpoints: TDABBreakpointList read GetBreakpoints;
  end;

  TDABBreakpointsResponse = class(TDABResponse)
  private
    function GetBody: TDABBreakpointsResponseBody;
  public
    destructor Destroy; override;
  published
    property Body: TDABBreakpointsResponseBody read GetBody;
  end;

  { TDABThread }

  TDABThread = class
  private
    FId: Integer;
    FName: string;
  published
    property Id: Integer read FId write FId;
    property Name: string read FName write FName;
  end;
  TDABThreadList = specialize TObjectList<TDABThread>;
  TDABThreadListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABThread>;

  { TDABThreadsResponseBody }

  TDABThreadsResponseBody = class
  private
    FThreads: TDABThreadList;
    function GetThreads: TDABThreadList;
  public
    destructor Destroy; override;
  published
    property Threads: TDABThreadList read GetThreads;
  end;

  TDABThreadsResponse = class(TDABResponse)
  private
    function GetBody: TDABThreadsResponseBody;
  public
    destructor Destroy; override;
  published
    property Body: TDABThreadsResponseBody read GetBody;
  end;

  { TDABEvent }

  TDABEvent = class(TDABProtocolMessage)
  private
    FBody: TObject;
    FEvent: string;
  public
    constructor Create; override;
  published
    property Event: string read FEvent write FEvent;
    property Body: TObject read FBody write FBody;
  end;

  { TDabInitializedEvent }

  TDABInitializedEvent = class(TDABEvent)
  public
    constructor create; override;
  end;

  { TDABStoppedEventBody }

  TDABStoppedEventBody = class
  private
    FReason: string;
    FDescription: string;
    FThreadId: Integer;
    FText: string;
    FHitBreakpointIds: TDynIntegerArray;
  published
    property Reason: string read FReason write FReason;
    property Description: string read FDescription write FDescription;
    property ThreadId: Integer read FThreadId write FThreadId;
    property Text: string read FText write FText;
    property HitBreakpointIds: TDynIntegerArray read FHitBreakpointIds write FHitBreakpointIds;
  end;

  { TDABStoppedEvent }

  TDABStoppedEvent = class(TDABEvent)
  private
    function GetBody: TDABStoppedEventBody;
  public
    constructor create; override;
    destructor Destroy; override;
  published
    property Body: TDABStoppedEventBody read GetBody;
  end;

  { TDABExitedEventBody }

  TDABExitedEventBody = class
  private
    FExitCode: Integer;
  published
    property ExitCode: Integer read FExitCode write FExitCode;
  end;

  { TDABExitedEvent }

  TDABExitedEvent = class(TDABEvent)
  public
    constructor create; override;
    destructor Destroy; override;
  end;

  { TDABTerminatedEvent }

  TDABTerminatedEvent = class(TDABEvent)
  public
    constructor create; override;
  end;

  { TDABOutputEventBody }

  TDABOutputEventBody = class
  private
    FCategory: string;
    FOutput: string;
  published
    property Category: string read FCategory write FCategory;
    property Output: string read FOutput write FOutput;
  end;

  TDABOutputEvent = class(TDABEvent)
  private
    function GetBody: TDABOutputEventBody;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property Body: TDABOutputEventBody read GetBody;
  end;

  { TDABModule }

  TDABModule = class
  private
    FId: Integer;
    FName: string;
    FPath: string;
    FSymbolStatus: string;
  published
    property Id: Integer read FId write FId;
    property Name: string read FName write FName;
    property Path: string read FPath write FPath;
    property SymbolStatus: string read FSymbolStatus write FSymbolStatus;
  end;

  { TDABModuleEventBody }

  TDABModuleEventBody = class
  private
    FReason: string;
    FModule: TDABModule;
    function GetModule: TDABModule;
  public
    destructor Destroy; override;
  published
    property Reason: string read FReason write FReason;
    property Module: TDABModule read GetModule;
  end;

  TDABModuleEvent = class(TDABEvent)
  private
    function GetBody: TDABModuleEventBody;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property Body: TDABModuleEventBody read GetBody;
  end;

  { TDABBreakpointEventBody }

  TDABBreakpointEventBody = class
  private
    FReason: string;
    FBreakpoint: TDABBreakpoint;
    function GetBreakpoint: TDABBreakpoint;
  public
    destructor Destroy; override;
  published
    property Reason: string read FReason write FReason;
    property Breakpoint: TDABBreakpoint read GetBreakpoint;
  end;

  { TDABBreakpointEvent }

  TDABBreakpointEvent = class(TDABEvent)
  private
    function GetBody: TDABBreakpointEventBody;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property Body: TDABBreakpointEventBody read GetBody;
  end;


implementation

{ TDABBreakpointEventBody }

function TDABBreakpointEventBody.GetBreakpoint: TDABBreakpoint;
begin
  if not Assigned(FBreakpoint) then
    FBreakpoint := TDABBreakpoint.Create;
  Result := FBreakpoint;
end;

destructor TDABBreakpointEventBody.Destroy;
begin
  FBreakpoint.Free;
  inherited Destroy;
end;

{ TDABBreakpointEvent }

function TDABBreakpointEvent.GetBody: TDABBreakpointEventBody;
begin
  Result := FBody as TDABBreakpointEventBody;
end;

constructor TDABBreakpointEvent.Create;
begin
  inherited Create;
  FEvent := 'breakpoint';
  FBody := TDABBreakpointEventBody.Create;
end;

destructor TDABBreakpointEvent.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABModuleEventBody }

function TDABModuleEventBody.GetModule: TDABModule;
begin
  if not Assigned(FModule) then
    FModule := TDABModule.Create;
  Result := FModule;
end;

destructor TDABModuleEventBody.Destroy;
begin
  FModule.Free;
  inherited Destroy;
end;

{ TDABModuleEvent }

function TDABModuleEvent.GetBody: TDABModuleEventBody;
begin
  Result := FBody as TDABModuleEventBody;
end;

constructor TDABModuleEvent.Create;
begin
  inherited Create;
  FEvent := 'module';
  FBody := TDABModuleEventBody.Create;
end;

destructor TDABModuleEvent.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABThreadsResponse }

destructor TDABThreadsResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

function TDABThreadsResponse.GetBody: TDABThreadsResponseBody;
begin
  if not assigned(FBody) then
    FBody := TDABThreadsResponseBody.Create;
  result := FBody as TDABThreadsResponseBody;
end;

{ TDABInitializeRequest }

function TDABInitializeRequest.GetArguments: TDABInitializeRequestArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABInitializeRequestArguments.Create;
  Result := FArguments as TDABInitializeRequestArguments;
end;

{ TDABAttachRequest }

function TDABAttachRequest.GetArguments: TDABAttachRequestArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABAttachRequestArguments.Create;
  Result := FArguments as TDABAttachRequestArguments;
end;

{ TDABScopesResponse }

destructor TDABScopesResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

function TDABScopesResponse.GetBody: TDABScopesResponseBody;
begin
  if not Assigned(FBody) then
    FBody := TDABScopesResponseBody.Create;
  Result := FBody as TDABScopesResponseBody;
end;

{ TDABLaunchRequestArgumentEnvironmentVariable }

constructor TDABLaunchRequestArgumentEnvironmentVariable.Create(AName, AValue: string);
begin
  FName := AName;
  FValue := AValue;
end;

{ TDABVariablesResponse }

function TDABVariablesResponse.GetBody: TDABVariablesResponseBody;
begin
  if not Assigned(FBody) then
    FBody := TDABVariablesResponseBody.Create;
  Result := FBody as TDABVariablesResponseBody;
end;

destructor TDABVariablesResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABEvaluateResponse }

function TDABEvaluateResponse.GetBody: TDABEvaluateResponseBody;
begin
  if not Assigned(FBody) then
    FBody := TDABEvaluateResponseBody.Create;
  Result := FBody as TDABEvaluateResponseBody;
end;

destructor TDABEvaluateResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABStackTraceResponse }

function TDABStackTraceResponse.GetBody: TDABStackTraceResponseBody;
begin
  if not assigned(FBody) then
    FBody := TDABStackTraceResponseBody.Create;
  Result := FBody as TDABStackTraceResponseBody;
end;

destructor TDABStackTraceResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABBreakpointsResponse }

function TDABBreakpointsResponse.GetBody: TDABBreakpointsResponseBody;
begin
  if not assigned(FBody) then
    FBody := TDABBreakpointsResponseBody.Create;
  result := FBody as TDABBreakpointsResponseBody;
end;

destructor TDABBreakpointsResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABSetVariableRequest }

function TDABSetVariableRequest.GetArguments: TDABSetVariableArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABSetVariableArguments.Create;
  Result := FArguments as TDABSetVariableArguments;
end;

{ TDABLaunchRequestArguments }

function TDABLaunchRequestArguments.GetEnvironment: TDABLaunchRequestArgumentEnvironmentVariableList;
begin
  if not Assigned(FEnvironment) then
    FEnvironment := TDABLaunchRequestArgumentEnvironmentVariableList.Create(True);
  Result := FEnvironment;
end;

destructor TDABLaunchRequestArguments.Destroy;
begin
  FEnvironment.Free;
  inherited Destroy;
end;

{ TDABEvaluateResponseBody }

function TDABEvaluateResponseBody.GetPresentationHint: TDABVariablePresentationHint;
begin
  if not Assigned(FPresentationHint) then
    FPresentationHint := TDABVariablePresentationHint.Create;
  Result := FPresentationHint;
end;

destructor TDABEvaluateResponseBody.Destroy;
begin
  FPresentationHint.Free;
  inherited Destroy;
end;

{ TDABVariable }

destructor TDABVariable.Destroy;
begin
  FPresentationHint.Free;
  inherited Destroy;
end;

function TDABVariable.GetPresentationHint: TDABVariablePresentationHint;
begin
  if not Assigned(FPresentationHint) then
    FPresentationHint := TDABVariablePresentationHint.Create;
  Result := FPresentationHint;
end;

{ TDABEvaluateRequest }

function TDABEvaluateRequest.GetArguments: TDABEvaluateArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABEvaluateArguments.Create;
  Result := FArguments as TDABEvaluateArguments;
end;

{ TDABOutputEvent }

constructor TDABOutputEvent.Create;
begin
  inherited create;
  FEvent := 'output';
  FBody := TDABOutputEventBody.Create;
end;

destructor TDABOutputEvent.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

function TDABOutputEvent.GetBody: TDABOutputEventBody;
begin
  Result := FBody as TDABOutputEventBody;
end;

{ TDABVariablesResponseBody }

destructor TDABVariablesResponseBody.Destroy;
begin
  FVariables.Free;
  inherited Destroy;
end;

function TDABVariablesResponseBody.GetVariables: TDABVariableList;
begin
  if not Assigned(FVariables) then
    FVariables := TDABVariableList.Create(True);
  Result := FVariables;
end;

{ TDABVariablesRequest }

function TDABVariablesRequest.GetArguments: TDABVariablesArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABVariablesArguments.Create;
  Result := FArguments as TDABVariablesArguments;
end;

{ TDABTerminatedEvent }

constructor TDABTerminatedEvent.create;
begin
  inherited create;
  FEvent := 'terminated';
end;

{ TDABScopesResponseBody }

function TDABScopesResponseBody.GetScopes: TDABScopeList;
begin
  if not Assigned(FScopes) then
    FScopes := TDABScopeList.Create(True);
  Result := FScopes;
end;

destructor TDABScopesResponseBody.Destroy;
begin
  FScopes.Free;
  inherited Destroy;
end;

{ TDABScopesRequest }

function TDABScopesRequest.GetArguments: TDABScopesRequestArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABScopesRequestArguments.Create;
  Result := FArguments as TDABScopesRequestArguments;
end;

{ TDABStackTraceResponseBody }

function TDABStackTraceResponseBody.GetStackFrames: TDABStackFrameList;
begin
  if not Assigned(FStackFrames) then
    FStackFrames := TDABStackFrameList.Create(True);
  Result := FStackFrames;
end;

destructor TDABStackTraceResponseBody.Destroy;
begin
  FStackFrames.Free;
  inherited Destroy;
end;

{ TDABStackFrame }

destructor TDABStackFrame.Destroy;
begin
  FSource.Free;
  inherited Destroy;
end;

procedure TDABStackFrame.SetSource(AValue: TDABSource);
begin
  if FSource=AValue then Exit;
  if Assigned(FSource) then
    FSource.Free;
  FSource:=AValue;
end;

{ TDABStackTraceRequest }

function TDABStackTraceRequest.GetArguments: TDABStackTraceArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABStackTraceArguments.Create;
  Result := FArguments as TDABStackTraceArguments;
end;

{ TDABStoppedEvent }

constructor TDABStoppedEvent.create;
begin
  inherited create;
  FEvent := 'stopped';
  FBody := TDABStoppedEventBody.create;
end;

destructor TDABStoppedEvent.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

function TDABStoppedEvent.GetBody: TDABStoppedEventBody;
begin
  Result := FBody as TDABStoppedEventBody;
end;

{ TDABThreadsResponseBody }

destructor TDABThreadsResponseBody.Destroy;
begin
  FThreads.Free;
  inherited Destroy;
end;

function TDABThreadsResponseBody.GetThreads: TDABThreadList;
begin
  if not Assigned(FThreads) then
    FThreads := TDABThreadList.Create(True);
  Result := FThreads;
end;

{ TDABExitedEvent }

constructor TDABExitedEvent.create;
begin
  inherited create;
  FEvent := 'exited';
  FBody := TDABExitedEventBody.Create;
end;

destructor TDABExitedEvent.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDABBreakpointsResponseBody }

function TDABBreakpointsResponseBody.GetBreakpoints: TDABBreakpointList;
begin
  if not Assigned(FBreakpoints) then
    FBreakpoints := TDABBreakpointList.Create(True);
  Result := FBreakpoints;
end;

destructor TDABBreakpointsResponseBody.Destroy;
begin
  FBreakpoints.Free;
  inherited Destroy;
end;

{ TDABSetBreakpointsRequest }

function TDABSetBreakpointsRequest.GetArguments: TDabSetBreakpointsArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDabSetBreakpointsArguments.Create;
  Result := FArguments as TDabSetBreakpointsArguments;
end;

{ TDabSetBReakpointsArguments }

function TDABSetBReakpointsArguments.GetBreakpoints: TDABSourceBreakpointList;
begin
  if not Assigned(FBreakpoints) then
    FBreakpoints := TDABSourceBreakpointList.Create(True);
  Result := FBreakpoints;
end;

destructor TDABSetBReakpointsArguments.Destroy;
begin
  FBreakpoints.Free;
  FSource.Free;
  inherited Destroy;
end;

function TDABSetBreakpointsArguments.GetSource: TDabSource;
begin
  if not Assigned(FSource) then
    FSource := TDabSource.Create;
  Result := FSource;
end;

{ TDabInitializedEvent }

constructor TDABInitializedEvent.create;
begin
  inherited create;
  FEvent := 'initialized';
end;

{ TDABEvent }

constructor TDABEvent.create;
begin
  inherited create;
  FType := 'event';
end;

{ TDABError }

function TDABError.GetError: TDABMessage;
begin
  if not Assigned(FError) then
    FError := TDABMessage.Create;
  Result := FError;
end;

destructor TDABError.Destroy;
begin
  FError.Free;
  inherited Destroy;
end;

{ TDABErrorResponse }

function TDABErrorResponse.GetBody: TDABError;
begin
  if not assigned(FBody) then
    FBody := TDABError.Create;
  result := FBody as TDABError;
end;

destructor TDABErrorResponse.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TDabLaunchRequest }

function TDABLaunchRequest.GetArguments: TDABLaunchRequestArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDabLaunchRequestArguments.Create;
  Result := FArguments as TDabLaunchRequestArguments;
end;

{ TDABResponse }

constructor TDABResponse.create;
begin
  FType := 'response';
end;

{ TDABRequest }

constructor TDABRequest.create;
begin
  FType := 'request';
end;

destructor TDABRequest.Destroy;
begin
  FArguments.Free;
  inherited Destroy;
end;

{ TDABProtocolMessage }

constructor TDABProtocolMessage.Create;
begin
  //
end;

var
  GSeqNr: integer = 0;

procedure TDABProtocolMessage.InitSequence;
begin
  inc(GSeqNr);
  FSeq := GSeqNr;
end;

initialization
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDabSourceBreakpointListSerializer, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDabBreakpointListSerializer, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDABThreadListSerializer, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDABStackFrameListSerializer, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDABScopeListSerializer, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDABVariableListSerializer, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDABLaunchRequestArgumentEnvironmentVariableListSerializer, 100);
end.

