# FPCDab

Free Pascal definitons for Microsoft's Debug Adapter Protocol. ([DAB](https://microsoft.github.io/debug-adapter-protocol))

### Installation

Installation can be done from the central fppkg-repository:

    fppkg install fpcdab

It is also possible to download the sources, start a command-line and make the source-directory the current-directory. Then type:

    fppkg install

Now all units from the FPCDab-package can be used in your Free Pascal applications.

## Contributing

Questions, ideas and patches are welcome. You can send them directly to <joost@cnoc.nl>, or discuss them at the fpc-pascal mailinglist.

## Versioning

This package is very new and receives a lot of updates. For the versions available, see the tags on this repository.

## Authors

* **Joost van der Sluis** - *initial work* - <joost@cnoc.nl>

License
=======
This library is distributed under the Library GNU General Public License version 2 (see the [COPYING.LGPL.txt](COPYING.LGPL.txt) file) with the following modification:

- object files and libraries linked into an application may be distributed without source code. As further eplained in the file [COPYING.modifiedLGPL.txt](COPYING.modifiedLGPL.txt).