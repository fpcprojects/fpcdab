unit DABFpdMessages;

{$mode ObjFPC}{$H+}

interface

uses
  sysutils,
  Generics.Collections,
  csModel,
  csGenericCollectionsDescriber,
  DABMessages;

type
  TDABFpdVariableSortStrategy = (
    Defined,          // Sort based on order of definition
    Visibility,       // Sort based on visibility
    InheritenceDepth, // Sort based on the inheritence-level of the structure a member is defined in
    Alphabetic        // Sort alphabetically
  );

  TDABFpdVariableSortOrder = (
    Ascending,
    Descending
  );

  { TDABFpdOrderBy }

  TDABFpdOrderBy = class
  private
    FOrder: TDABFpdVariableSortOrder;
    FStrategy: TDABFpdVariableSortStrategy;
  published
    property Strategy: TDABFpdVariableSortStrategy read FStrategy write FStrategy;
    property Order: TDABFpdVariableSortOrder read FOrder write FOrder;
  end;
  TDABFpdOrderByList = specialize TObjectList<TDABFpdOrderBy>;
  TDABFpdOrderByListSerializer = specialize TcsGenericCollectionTListObjectDescriber<TDABFpdOrderBy>;

  { TDABFpdLaunchRequestArguments }

  TDABFpdLaunchRequestArguments = class(TDABLaunchRequestArguments)
  private
    FForceNewConsoleWin: Boolean;
    FProgram: string;
    FParameters: TStringArray;
    FWorkingDirectory: string;
    FEnvironment: TDABLaunchRequestArgumentEnvironmentVariableList;
    FOrderVariablesBy: TDABFpdOrderByList;
    function GetEnvironment: TDABLaunchRequestArgumentEnvironmentVariableList;
    function GetOrderVariablesBy: TDABFpdOrderByList;
  public
    destructor Destroy; override;
  published
    property &Program: string read FProgram write FProgram;
    property Parameters: TStringArray read FParameters write FParameters;
    property WorkingDirectory: string read FWorkingDirectory write FWorkingDirectory;
    property Environment: TDABLaunchRequestArgumentEnvironmentVariableList read GetEnvironment;
    property ForceNewConsoleWin: Boolean read FForceNewConsoleWin write FForceNewConsoleWin;
    property OrderVariablesBy: TDABFpdOrderByList read GetOrderVariablesBy;
  end;

  TDABFpdLaunchRequest = class(TDABLaunchRequest)
  private
    function GetArguments: TDABFpdLaunchRequestArguments;
  published
    property Arguments: TDABFpdLaunchRequestArguments read GetArguments;
  end;

implementation

{ TDABFpdLaunchRequest }

function TDABFpdLaunchRequest.GetArguments: TDABFpdLaunchRequestArguments;
begin
  if not Assigned(FArguments) then
    FArguments := TDABFpdLaunchRequestArguments.Create;
  Result := FArguments as TDABFpdLaunchRequestArguments;
end;

{ TDABFpdLaunchRequestArguments }

function TDABFpdLaunchRequestArguments.GetOrderVariablesBy: TDABFpdOrderByList;
begin
  if not Assigned(FOrderVariablesBy) then
    FOrderVariablesBy := TDABFpdOrderByList.Create(True);
  Result := FOrderVariablesBy;
end;

function TDABFpdLaunchRequestArguments.GetEnvironment: TDABLaunchRequestArgumentEnvironmentVariableList;
begin
  if not Assigned(FEnvironment) then
    FEnvironment := TDABLaunchRequestArgumentEnvironmentVariableList.Create(True);
  Result := FEnvironment;
end;

destructor TDABFpdLaunchRequestArguments.Destroy;
begin
  FEnvironment.Free;
  FOrderVariablesBy.Free;
  inherited Destroy;
end;

initialization
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TDABFpdOrderByListSerializer, 100);
end.

